from cffi import FFI
from rspy_interface import lib
import numpy as np

ffi = FFI()

np_array = np.int32([1, 2, 3, 4])
print(np_array)
pointer = ffi.cast("int *", np_array.ctypes.data)
p = lib.sum_int(pointer, len(np_array))
print(p)

np_array = np.int64([1, 2, 3, 4])
print(np_array)
pointer = ffi.cast("long *", np_array.ctypes.data)
p = lib.sum_long(pointer, len(np_array))
print(p)

np_array = np.float32([1., 2., 3., 4.])
print(np_array)
pointer = ffi.cast("float *", np_array.ctypes.data)
print(lib.sum_float(pointer, len(np_array)))

np_array = np.float64([1., 2., 3., 4.])
print(np_array)
pointer = ffi.cast("double *", np_array.ctypes.data)
print(lib.sum_double(pointer, len(np_array)))

# ------------------
print("array increments")
np_array = np.float64([1., 2., 3., 4.])
print(np_array)
pointer = ffi.cast("double *", np_array.ctypes.data)
lib.double_increase_by_one(pointer, len(np_array))
print(np_array)

np_array = np.int64([1., 2., 3., 4.])
print(np_array)
pointer = ffi.cast("long *", np_array.ctypes.data)
lib.long_increase_by_one(pointer, len(np_array))
print(np_array)
