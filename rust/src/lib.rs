use std::slice;
use std::os::raw::{c_int, c_long, c_float, c_double, c_uint};

#[no_mangle]
pub unsafe extern "C" fn sum_int(array: *const c_int, length: c_uint) -> c_int {
    assert!(!array.is_null(), "Null pointer in sum()");

    let array: &[c_int] = slice::from_raw_parts(array, length as usize);
    array.into_iter().sum()
}

#[no_mangle]
pub unsafe extern "C" fn sum_long(array: *const c_long, length: c_uint) -> c_long {
    assert!(!array.is_null(), "Null pointer in sum()");

    let array: &[c_long] = slice::from_raw_parts(array, length as usize);
    array.into_iter().sum()
}

#[no_mangle]
pub unsafe extern "C" fn sum_float(array: *const c_float, length: c_uint) -> c_float {
    assert!(!array.is_null(), "Null pointer in sum()");

    let array: &[c_float] = slice::from_raw_parts(array, length as usize);
    array.into_iter().sum()
}

#[no_mangle]
pub unsafe extern "C" fn sum_double(array: *const c_double, length: c_uint) -> c_double {
    assert!(!array.is_null(), "Null pointer in sum()");

    let array: &[c_double] = slice::from_raw_parts(array, length as usize);
    array.into_iter().sum()
}

#[no_mangle]
pub unsafe extern "C" fn double_increase_by_one(n: *mut c_double, len: usize){
    let numbers = {
        assert!(!n.is_null());

        slice::from_raw_parts_mut(n, len)
    };

    for i in 0..len{
        numbers[i] += 1.;
    }
}

#[no_mangle]
pub unsafe extern "C" fn long_increase_by_one(n: *mut c_long, len: usize){
    let numbers = {
        assert!(!n.is_null());

        slice::from_raw_parts_mut(n, len as usize)
    };

    for i in 0..len{
        numbers[i] += 1;
    }
}
